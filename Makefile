build:
	SLIGHT_BRIGHTNESS_DIR=$(PWD)/ rustc --edition 2021 -C prefer-dynamic=yes -g slight.rs

slight: slight.rs
	rustc --edition 2021 -C opt-level=s -C strip=symbols slight.rs

release: slight



DEST := /usr/local/bin/

install: release
	 mkdir -p $(DEST)
	 cp slight $(DEST)

uninstall:
	rm $(DEST)/slight

clean:
	rm -f slight
	