# Slight
A stupid simple program to set screen brightness on linux comfortably :3

I made this because I couldn't find any brightness programs in gentoo's repositories, and constantly going into root just to change brighness was annoying.
There are no premade binaries, you have to [compile](#installing) it yourself.

# Usage
```sh
slight # print brightness info
slight 10 # set brightness to the absolute value 10
slight 40% # set brightness to 40% of maximum
slight -10% # decrease brightness by 10% of maximum
slight +3 # increase brightness by 3
```
To set brightness, you will need to run it as root.
This should be secure, as the program has extremely basic capabilities, and the worst an attacker could do is turn off your screen by changing the brightness to 0 (which by Linux security standards is very tame).
You may be able to make this program run when a certain key is pressed (such as `XF86MonBrightnessUp`) by configuring your DE/WM; If so, [passwordless automatic root](#passwordless-automatic-root) is required.


# Installing
This program must be compiled from source, because it has hardcoded system brighness interfaces. No prior rust knowledge is needed.

## Dependencies
- rustc. I developed this with version 1.66, but if your version of rustc is older there's a good chance it will still work anyway. You don't need cargo.
- GNUmake / gmake (may well be called just `make` in your package manager)
- bash

## Finding your brightness file

To compile, you need to have the `SLIGHT_BRIGHTNESS_DIR` env var set. This just points to a directory in `/sys/class/backlight/`.

You can use a script to set it automatically. From your shell, run:
```sh
. set_brightness_dir.sh
```
This will interactively ask you to choose a brightness interface in your kernel.
Any should work, if you have multiple and one doesn't work try recompiling with a different one.
If slight cannot change brightness and there is only one choice, then you either have rubbish hardware or aren't running it as root, and might want to try a different tool that jumps through more hoops.

## Compiling
In the directory, in the same terminal you selected your brightness interface:
```sh
make release
sudo make install
```

If the software does not work, let me know! In the mean time, you can use
```sh
make uninstall
```
to undo the `make install` command.

## Passwordless Automatic root
There are three ways of configuring this. If in doubt, SUID is the easiest, and sudo/doas are the most flexible.
### SUID bit
This is (probably) completely safe, as slight _shouldn't_ interact with the system at all outside of the brightness files.
It's better to set this with sudo/doas config files, because they let you choose who has access, whereas this will give *any* user on the system the right to change the brightness at will.
Regardless, if you don't care and want something quick and dirty:

```sh
chown root:root /usr/local/bin/slight
chmod +s /usr/local/bin/slight
```

### Sudo
```bash
# ~/.bashrc
alias slight='sudo slight'
```
Then allow users of the sudo/wheel group to run slight without password.
```sh
# /etc/sudoers
%sudo ALL=(ALL) NOPASSWD: /usr/local/bin/slight
%wheel ALL=(ALL) NOPASSWD: /usr/local/bin/slight
```

### Doas
```bash
# ~/.bashrc
alias slight='doas slight'
```
Then allow - for example - users of the wheel group, and the user called "bob" to run slight without password.
```sh
# /etc/doas.conf
permit nopass :wheel cmd slight
permit nopass bob cmd slight
```

## Apparmor profile (optional)
A sample apparmor profile is included in [`usr.local.bin.slight`](usr.local.bin.slight).
You will almost certainly need to edit it yourself to get it working, because apparmor hates symlinks.

# Contributing
Patches welcome! Submit them with [git send-email](https://git-send-email.io/) to `echo 'cGh5dG8rcGF0Y2hAZGlzcm9vdC5vcmc=' | base64 -d` or by pull request.
- Please drop me an email before you make substantial changes.
- If you encounter a bug, please report it! I promise I wont hate you for it!
- Remember to `rustfmt --config hard_tabs=true slight.rs` before committing!

