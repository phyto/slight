use std::error::Error;
use std::io;

// Get the brightness directory to use at compile time
const BRIGHTNESS_DIR: &str = env!(
	"SLIGHT_BRIGHTNESS_DIR",
	"No brightness directory specified. Please refer to the README compilation instructions."
);

fn main() -> Result<(), Box<dyn Error>> {
	let args: Vec<String> = std::env::args().collect();

	match args.len() {
		1 => print_status()?,
		2 => match parse_val(&args[1]) {
			Ok(s) => match set_val(s) {
				Ok(()) => print_status()?,
				Err(e) => println!("Error setting brighness: {e}"),
			},
			Err(e) => {
				println!("Error calculating brightness string '{}', {e}.\n", args[1]);
				print_usage();
			}
		},

		_ => print_usage(),
	}

	Ok(())
}

/// Convert human-friendly format into absolute brightness value: you may use percentages instead
/// of absolutes, and you may use increment/decrementers. Valid format is
/// `[+|-]{number}[%]`, so for example `20%` would set the brightness to 20% of the maximum, `+20%`
/// would then increase it to 40% of the maximum.
fn parse_val(s: &str) -> Result<usize, Box<dyn Error>> {
	let num = s
		.trim_start_matches('+')
		.trim_start_matches('-')
		.trim_end_matches('%');
	let mut val: usize = num.parse()?;

	if s.ends_with('%') {
		let max_v = get_max_val()? as f32;
		let percent = (val as f32 * max_v) / 100.0;

		val = percent.round() as usize;
	}

	if s.starts_with('+') {
		val += get_val()?;
	} else if s.starts_with('-') {
		val = get_val()?.saturating_sub(val);
	}

	// Don't try to increase brightness beyond max
	val = std::cmp::min(val, get_max_val()?);

	Ok(val)
}

fn print_status() -> io::Result<()> {
	let val = get_val()?;
	let max_val = get_max_val()?;
	let percent = (val as f32 / max_val as f32) * 100.0;
	println!("{val} of {max_val}  \t{}%", percent.round());
	Ok(())
}

/// Set the backlight brightness to `value`
fn set_val(value: usize) -> io::Result<()> {
	use std::fs::File;
	use std::io::Write;

	let mut filename = BRIGHTNESS_DIR.to_string();
	filename.push_str("/brightness");

	let mut f = match File::options().write(true).open(filename) {
		Ok(f) => f,
		Err(e) => {
			eprintln!("brightness file is not writeable: {e}");
			return Err(e);
		}
	};

	//FIXME: For normal fs files (like when debugging), this only overwrites the start of the file,
	//doesn't completely replace it.
	f.write_all(format!("{}", value).as_bytes())?;

	Ok(())
}

fn get_val() -> io::Result<usize> {
	let mut filename = BRIGHTNESS_DIR.to_string();
	filename.push_str("/brightness");

	let f = match std::fs::read_to_string(filename) {
		Ok(f) => f,
		Err(e) => {
			eprintln!("brightness file is not readable: {e}");
			return Err(e);
		}
	};

	Ok(f.trim().parse().expect(
		"brightness not an integer; SLIGHT_BRIGHTNESS_DIR was likely set wrong at compile time",
	))
}

fn get_max_val() -> io::Result<usize> {
	let mut filename = BRIGHTNESS_DIR.to_string();
	filename.push_str("/max_brightness");

	let f = match std::fs::read_to_string(filename) {
		Ok(f) => f,
		Err(e) => {
			eprintln!("max_brightness file is not readable: {e}");
			return Err(e);
		}
	};

	Ok(f.trim().parse().expect(
		"max brightness not an integer; SLIGHT_BRIGHTNESS_DIR was likely set wrong at compile time",
	))
}

fn print_usage() {
	let arg0 = std::env::args().next().unwrap();
	println!("Usage: {arg0} [[+|-]number[%]]");
	println!("");
	println!("Compiled with brightness directory SLIGHT_BRIGHTNESS_DIR={BRIGHTNESS_DIR}");
	print!("slight v1\t");
	println!("Source available at https://git.disroot.org/phyto/slight")
}
