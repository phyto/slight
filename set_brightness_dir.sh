#!/bin/bash

# Beware: I have absolutely no idea what i am doing
# I am not liable for any brain aneurysms incured during the reading of this source code.

root=/sys/class/backlight/
i=0

echo "The following backlight interfaces are available:"
echo

for dir in $root*/
do
  echo "  [$i]  $dir"
	dirs[$i]=" $dir"
	i=$(( $i + 1 ))
done
echo


#echo "array: ${dirs[@]}"
#echo "Total: ${#dirs[@]}"


read -r -p "Which would you like to use? {0 .. $(( $i - 1 ))}: " ans

i=0

for dir in $root*/
do
	if [ "$i" == "$ans" ]
	then
		export SLIGHT_BRIGHTNESS_DIR="$dir"
		echo "Exported $dir as SLIGHT_BRIGHTNESS_DIR"
	fi
		i=$(( $i + 1 ))
done

if [ -z $SLIGHT_BRIGHTNESS_DIR ]
	then
	echo "WARNING: environment variable still not set; you have made an invalid selection"
fi
